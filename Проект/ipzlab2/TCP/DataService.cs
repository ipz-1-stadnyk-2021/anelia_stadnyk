﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace TCP
{
    class DataService
    {
        private readonly DataBase _dbContext;
        public DataService(DataBase dbContext)
        {
            _dbContext = dbContext;
        }



        public List<Users> GetAllUsers()
        {
            return _dbContext.Users.ToList();
        }


        public Users GetUser(string login, string password)
        {
            return _dbContext.Users.Where(el => el.Login.Equals(login) && el.Password.Equals(password)).FirstOrDefault();
        }



        public void InsertUser(Users user)
        {
            var userInDb = _dbContext.Users.FirstOrDefault(el => el.Login.Equals(user.Login));
            if (userInDb != null)
            {
                throw new ArgumentException("Логін існує в базі даних");
            }
            var query = @"INSERT INTO Users (Login, Password) VALUES (@Login, @Password)";
            _dbContext.Database.ExecuteSqlCommand(query, new SqlParameter("@Login", user.Login), new SqlParameter("@Password", user.Password));
            var userId = _dbContext.Users.FirstOrDefault(el => el.Login.Equals(user.Login)).UserId;
        }

        public List<string> SearchByNumber(int number)
        {
            List<string> transport = _dbContext.Database.SqlQuery<string>("SELECT TransportName FROM Transport WHERE TransportName like '%" + number + "%'").ToList();
            return transport;
        }

        public List<string> SearchByStop(string stop)
        {
            List<string> transport = _dbContext.Database.SqlQuery<string>("SELECT TransportName from Transport t left JOIN Routes r ON t.TransportId = r.TransportId Right JOIN Stops s ON r.StopId = s.StopId  where StopName like '" + stop + "%'").ToList();
            return transport;
        }

        public List<string> SearchStops(string transport)
        {
            List<string> stops = _dbContext.Database.SqlQuery<string>("SELECT StopName from Transport t left JOIN Routes r ON t.TransportId = r.TransportId Right JOIN Stops s ON r.StopId = s.StopId  where TransportName = @Transport",
             new SqlParameter("@Transport", transport)).ToList();
            return stops;
        }
        
        public string GetPos(string transport)
        {
            string pos = _dbContext.Database.SqlQuery<string>("SELECT TransportLocation FROM Transport where TransportName = @Transport",
             new SqlParameter("@Transport", transport)).FirstOrDefault();
            return pos;
        }
    }
}
